/*
Last Modification Date: 2021-01-09
Author: Gilberto Noronha <gnoronha@burning-glass.com> 

Please see the "README" file for basic installation and usage
instructions.

*/

with base_table as
(select
distinct
c.occupation,
trim(replace(replace(b.value, '"', ''), '\'', '')) as skill,
d.skill_cluster,
d.skill_cluster_family
from 
bgt_web_apps_data_store.li_us.suboccskillcategory as a
inner join lateral 
  flatten(strtok_to_array(replace(replace(definingskills, 
  '[', ''), ']', ''), ',')) as b 
inner join (select distinct specialized_occupation, occupation 
  from bgt_derived_data.optimized_tables.job_posting 
  where country_code = 'US' and occupation is not null) as c
on a.occupation = c.specialized_occupation
inner join (select * from 
  bgt_derived_data.optimized_tables.skill_taxonomy 
  where country_code = 'US') as d 
  on trim(replace(replace(b.value, '"', ''), '\'', '')) = 
  d.skill_name
)

select
b.prediction_id,
a.occupation
from
base_table as a
inner join (select * from metadata where variable_type = 'skill') as b
  on a.skill = b.variable_name

union all
select
b.prediction_id,
a.occupation
from
(select distinct occupation, skill_cluster from base_table) as a
inner join (select * from metadata where 
  variable_type = 'skill_cluster') as b
  on a.skill_cluster = b.variable_name

union all
select
b.prediction_id,
a.occupation
from
(select distinct occupation, skill_cluster_family from base_table) as a
inner join (select * from metadata where 
  variable_type = 'skill_cluster_family') as b
  on a.skill_cluster_family = b.variable_name
