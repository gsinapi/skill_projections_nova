/*
Last Modification Date: 2021-01-09
Author: Gilberto Noronha <gnoronha@burning-glass.com>

Please see the "README" file for basic installation and usage
instructions.

*/

select
a.prediction_id,
b.specialized_occupation as occupation,
b.occupation_rank,
b.job_postings_count,
b.occupation_one_year_growth,
b.occupation_two_years_growth,
b.occupation_three_years_growth,
b.occupation_five_years_growth,
c.one_year_growth,
c.two_years_growth,
c.three_years_growth,
c.five_years_growth,
case when one_year_growth is null then 1 else
 case when occupation_one_year_growth >= 0 and
   one_year_growth >= 0 or occupation_one_year_growth <= 0
   and one_year_growth <= 0 then least((abs(div0(abs(one_year_growth),
   abs(occupation_one_year_growth)) - 1) / 20), 1) else 1 end end
 as svr_prediction_deviation
from
metadata as a
inner join
(
select
a.*,
b.one_year_growth / 100 as occupation_one_year_growth,
b.two_year_growth / 100 as occupation_two_years_growth,
b.three_year_growth / 100 as occupation_three_years_growth,
b.five_year_growth / 100 as occupation_five_years_growth,
'skill' as variable_type
from
(select
*,
dense_rank() over (partition by geography, variable_name
 order by job_postings_count desc) as occupation_rank
from
(select
a.country_code as geography,
a.specialized_occupation,
replace(b.value, '"', '') as variable_name,
count(*) as job_postings_count
from
identifier($skill_predictor_job_posting_table) as a
inner join lateral flatten(skills) as b
where
geography in ('US', 'GB', 'CA', 'AU', 'SG', 'NZ', 'ID')
and specialized_occupation is not null and variable_name is not null
and job_posting_date >= '2019-11-01' and job_posting_date <= '2020-10-31'
group by geography, variable_name, specialized_occupation)) as a
inner join (select * from BGT_TEMPORARY_DATA.RNARASIMHAN.EMPLOYMENT_PROJECTIONS_BY_SUBOCC
where geography_type = 'Nationwide') as b
on a.specialized_occupation = b.sub_occupation_name_en_us

union all
select
a.*,
b.one_year_growth / 100 as occupation_one_year_growth,
b.two_year_growth / 100 as occupation_two_years_growth,
b.three_year_growth / 100 as occupation_three_years_growth,
b.five_year_growth / 100 as occupation_five_years_growth,
'skill_cluster' as variable_type
from
(select
*,
dense_rank() over (partition by geography, variable_name
 order by job_postings_count desc) as occupation_rank
from
(select
a.country_code as geography,
a.specialized_occupation,
replace(b.value, '"', '') as variable_name,
count(*) as job_postings_count
from
identifier($skill_predictor_job_posting_table) as a
inner join lateral flatten(skill_clusters) as b
where
geography in ('US', 'GB', 'CA', 'AU', 'SG', 'NZ', 'ID')
and specialized_occupation is not null and variable_name is not null
and job_posting_date >= '2019-11-01' and job_posting_date <= '2020-10-31'
group by geography, variable_name, specialized_occupation)) as a
inner join (select * from BGT_TEMPORARY_DATA.RNARASIMHAN.EMPLOYMENT_PROJECTIONS_BY_SUBOCC
where geography_type = 'Nationwide') as b
on a.specialized_occupation = b.sub_occupation_name_en_us

union all
select
a.*,
b.one_year_growth / 100 as occupation_one_year_growth,
b.two_year_growth / 100 as occupation_two_years_growth,
b.three_year_growth / 100 as occupation_three_years_growth,
b.five_year_growth / 100 as occupation_five_years_growth,
'skill_cluster_family' as variable_type
from
(select
*,
dense_rank() over (partition by geography, variable_name
 order by job_postings_count desc) as occupation_rank
from
(select
a.country_code as geography,
a.specialized_occupation,
replace(b.value, '"', '') as variable_name,
count(*) as job_postings_count
from
identifier($skill_predictor_job_posting_table) as a
inner join lateral flatten(skill_cluster_families) as b
where
geography in ('US', 'GB', 'CA', 'AU', 'SG', 'NZ', 'ID')
and specialized_occupation is not null and variable_name is not null
and job_posting_date >= '2019-11-01' and job_posting_date <= '2020-10-31'
group by geography, variable_name, specialized_occupation)) as a
inner join (select * from BGT_TEMPORARY_DATA.RNARASIMHAN.EMPLOYMENT_PROJECTIONS_BY_SUBOCC
where geography_type = 'Nationwide') as b
on a.specialized_occupation = b.sub_occupation_name_en_us
) as b on a.geography = b.geography and a.variable_name = b.variable_name
and a.variable_type = b.variable_type
left join predicted_growth_svr as c
on a.prediction_id = c.prediction_id
