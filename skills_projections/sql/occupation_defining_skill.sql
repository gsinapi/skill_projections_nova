/*
Last Modification Date: 2021-01-09
Author: Gilberto Noronha <gnoronha@burning-glass.com>

Please see the "README" file for basic installation and usage
instructions.


(select * from
  bgt_derived_data.optimized_tables.skill_taxonomy
  where country_code = 'US')

  (select * from
    bgt_derived_data.optimized_tables_atlas.skill_taxonomy)


    (select
    distinct
    a.occupation,
    a.skill_name as skill,
    d.skill_cluster,
    d.skill_cluster_family
    from
    BGT_TEMPORARY_DATA.GSINAPI.DDN_PROCESSED as a
    inner join (select skill_name_en_us as skill_name, skill_id, skill_name_en_us as skill,
    skill_cluster_name_en_us as skill_cluster, skill_cluster_family_name_en_us as skill_cluster_family
    from BGT_DATA_34.PUBLIC.D_BGT_SKILL_CURRENT_PRODUCTION ) as d
      on a.skill_name =
      d.skill_name
    order by occupation)

*/

with base_table as
(select
distinct
a.occupation,
a.skill_name as skill,
d.skill_cluster,
d.skill_cluster_family
from
BGT_TEMPORARY_DATA.GSINAPI.DDN_PROCESSED as a
inner join (select * from
  bgt_derived_data.optimized_tables.skill_taxonomy
  where country_code = 'US') as d
  on a.skill_name =
  d.skill_name
order by occupation
)

select
b.prediction_id,
a.occupation
from
base_table as a
inner join (select * from metadata where variable_type = 'skill') as b
  on a.skill = b.variable_name

union all
select
b.prediction_id,
a.occupation
from
(select distinct occupation, skill_cluster from base_table) as a
inner join (select * from metadata where
  variable_type = 'skill_cluster') as b
  on a.skill_cluster = b.variable_name

union all
select
b.prediction_id,
a.occupation
from
(select distinct occupation, skill_cluster_family from base_table) as a
inner join (select * from metadata where
  variable_type = 'skill_cluster_family') as b
  on a.skill_cluster_family = b.variable_name
