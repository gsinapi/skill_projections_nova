#!/usr/bin/env python
#
# Last Modification Date: 2021-01-09
# Author: Gilberto Noronha <gnoronha@burning-glass.com> 
#
# Please see the "README" file for basic installation and usage
# instructions.
#

import logging

logger = logging.getLogger(__name__)

import datetime
import dateutil

import pandas as pd

def year_month_add(year_month, months):

  date = datetime.datetime.strptime(str(year_month),
                                    "%Y%m")

  result = date + \
    dateutil.relativedelta.relativedelta(months = months)

  return str(result)[:10]
        
def encode_dates(data):

  if "datetime" not in data.dtype.name:
    data = pd.to_datetime(data, format = "%Y%m")

  return data.values.astype("datetime64[D]")\
                               .astype(float)

