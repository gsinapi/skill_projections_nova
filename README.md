
**Run Code**

- install packages in the setup.py. Make sure to install the snowflake-helpers package. You can create a new conda environment named 'snowflake' and install all the needed packages
- activate the new conda environment
- in the command_line.py file (in the skill_projections folder), modify the destination schema of the tables in the main(), namely development_schema, where all the intermediate and final tables will be stored
- Also modify the snowflake configuration file (skills_projections/templates/snowflake_config) to make sure to include the correct credentials for the connection
- Run the code as follows:
 %run skills_projections/command_line.py

**Model Pipeline**

 The following steps illustrates the tables produced during the model pipeline, using the homonym sql queries included in the folder skill_projections/sql:

***SKILL_VARIABLES***: this table contains a (random generated) variable_id for each skill/skill_clusters/skill_cluster_family in each geography, with the associated skill cluster and skill cluster family  

***MONTHLY_SKILL_COUNTS***: this table contains monthly counts for every variable_id (where a variable_id represents a skill/skill cluster/skill cluster family available in all geographies. The variable_id for the same skills in different geographies should be different.). The date format is year_month (for example, 201901 for January 2019). The table includes also other metrics, such as a rolling average and a deviation from the rolling average, which are used to calculate the adjusted job posting count.

***SKILL_QUALITY_METRICS***: this table contains quality metrics for every variable_id and it is based on the monthly skill counts. The quality metrics determine the ‘goodness’ of a skills, which means whether a skill passes or not the quality checks and is eligible to be projected.  

***METADATA***: table that contains quality metrics to determine goodness of skills, as well as first valid dates (year/month). The quality metrics are calculated based on bgt data and stackoverflow data and determine whether a skill has passed quality checks.  

***PREPARED INPUT***: table that creates the input for the SVR model, producing an aggregate index for each skill to be projected, where the aggregate index is a combination of bgt and stackoverflow indexes, created using the job postings count by year/month for the skills that passed the quality checks. In case a stackoverflow index is not available (skill not present in stackoverflow data), the bgt index only will be used.  


***PREDICTED LEVEL SVR***: train the machine learning model and predict the aggregate index for each year/month using SVR model.  


***PREDICTED GROWTH SVR***: Using the output from svr model, this step creates the growth rates (1,2,3,5 years growth), calculating the percentage difference between the predicted values and base values (based on the aggregate index).  


***OCCUPATION DEFINING SKILLS***: Using ad-hoc list of defining and defining++ skills for each bgt occupation, identify skills that ‘defines’ occupations. The table will have, for each skill, a list of occupations that are defined by such skill.  


 ***SKILL OCCUPATION COUNTS***: for each skill-occ combination,  this step retrieves the following information: job postings count, occupations rank, occupation projection, svr skill projections, and calculates the svr prediction deviation. The svr prediction deviation represents the degree of deviation between the occupation projection defined by skill X and the associated svr skill X projection. This is an index that measures how much a given machine learning skill prediction deviates, on average, from the predictions of occupations defined by such skill. A cutoff value of this deviation is set as an acceptance criteria.  


***PREDICTED GROWTH DERIVED***: calculate ‘derived’ projections for each skill that defines occupations, using occupation projections. The derived projections for each skill are based on the occupation projections defined by such skill and the related job postings count. Also, a ‘defined_occupation_share’ variable is created, defined as the ratio between the number of job postings for each skill-occupation for occupations defined by such skill over the total number of skill occupation counts for that skill. This share is determined by the fraction of job postings (from the past 12 months) associated with skill X that are also associated with occupations defined by X.  


***PREDICTED GROWTH***: this table includes the final projections based on 4 different approaches:

 - SVR: projections produced by svr (machine learning model).  

 - DERIVED: projections produced by the derived skill projections approach (occ projections + defining skills). In case the deviation between svr results and derived results (svr prediction deviation) is > cutoff value, derived projections are selected as final projections, otherwise SVR projections are chosen.  

 - COMBINED: this type of results represents a combined approach between svr and derived predictions. Being more precise, suppose we are talking about skill X, and let S1 be the machine learning growth rate of X (svr), and let S2 be the corresponding "derived" growth rate. If S1, S2 > 0, S1 > S2, and D > C, where C is the deviaton cutoff mentioned in the previous section (currently, C = 0.62, as mentioned earlier), the combined growth rate is a weighted average of S1 and S2, with the weight of S2 being determined by the defined_occupation_share variable

 - OVERRIDE: this type of results is determined by the user by some business rules. Let’s say we want the skill Microsoft Office to trend as the skill Microsoft Excel with a certain weight (let’s say 0.6). Then the ‘override’ projected growth rate for Microsoft Office will be a weighted average of the projections of the 2 skills, namely (0.6*final projection for Microsoft Excel) + (0.4*candidate projection for Microsoft Office). A skill projection can also be linked to a skill cluster projection using the same approach.  
